core: 7.x
api: 2
projects:
  drupal:
    version: '7.22'
  views_bulk_operations:
    version: '3.1'
  addressfield:
    version: 1.0-beta3
  admin_menu:
    version: 3.0-rc4
  ais:
    version: '1.6'
  auto_entitylabel:
    version: '1.1'
  ctools:
    version: '1.3'
  calendar:
    version: '3.4'
  commerce:
    version: '1.5'
  commerce_autosku:
    version: '1.1'
  commerce_cheque:
    version: '1.0'
  commerce_custom_product:
    version: 1.0-beta2
  commerce_fieldgroup_panes:
    version: '1.0'
  commerce_paypal:
    version: '1.0'
  commerce_shipping:
    version: '2.0'
  commerce_vbo_views:
    version: '1.2'
  context:
    version: 3.0-beta6
  css_injector:
    version: '1.8'
  date:
    version: '2.6'
  diff:
    version: '3.2'
  ds:
    version: '2.2'
  fontyourface:
    version: '2.7'
  editablefields:
    version: 1.0-alpha2
  email:
    version: '1.2'
  entity:
    version: '1.0'
  entitycache:
    version: '1.2'
  entityreference:
    version: '1.0'
  field_group:
    version: '1.1'
  flexslider:
    version: 1.0-rc3
  formblock:
    version: 1.x-dev
  google_analytics:
    version: '1.3'
  hint:
    version: '1.2'
  honeypot:
    version: '1.14'
  imce:
    version: '1.7'
  imce_wysiwyg:
    version: '1.0'
  libraries:
    version: '2.1'
  link:
    version: '1.1'
  menu_attributes:
    version: 1.0-rc2
  menu_block:
    version: '2.3'
  module_filter:
    version: '1.7'
  mollom:
    version: '2.5'
  overlay_paths:
    version: '1.2'
  page_title:
    version: '2.7'
  path_breadcrumbs:
    version: 2.0-beta17
  pathauto:
    version: '1.2'
  phone:
    version: 1.x-dev
  publishcontent:
    version: '1.1'
  r4032login:
    version: '1.5'
  robotstxt:
    version: '1.2'
  rules:
    version: '2.3'
  stringoverrides:
    version: '1.8'
  token:
    version: '1.5'
  views:
    version: '3.6'
  views_accordion:
    version: 1.0-rc2
  views_field_view:
    version: '1.0'
  views_slideshow:
    version: '3.0'
  webform:
    version: '3.18'
  weight:
    version: '2.2'
  wysiwyg:
    version: '2.2'
  omega:
    version: '3.1'
  canada_price_format:
    type: module
    custom_download: true
  mobile_menu_toggle:
    type: module
    custom_download: true
  print:
    type: module
    custom_download: true
  print_epub:
    type: module
    custom_download: true
  print_epub_phpepub:
    type: module
    custom_download: true
  print_mail:
    type: module
    custom_download: true
  print_pdf:
    type: module
    custom_download: true
  print_pdf_dompdf:
    type: module
    custom_download: true
  print_pdf_mpdf:
    type: module
    custom_download: true
  print_pdf_tcpdf:
    type: module
    custom_download: true
  print_pdf_wkhtmltopdf:
    type: module
    custom_download: true
  print_ui:
    type: module
    custom_download: true
  session_expire:
    type: module
    custom_download: true
libraries:
  ckeditor:
    directory_name: ckeditor
    custom_download: true
    type: library
  fullcalendar:
    directory_name: fullcalendar
    custom_download: true
    type: library
  ckfinder:
    directory_name: ckfinder
    custom_download: true
    type: library
  flexslider:
    directory_name: flexslider
    custom_download: true
    type: library
