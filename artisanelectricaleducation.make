; ----------------
; Generated makefile from http://drushmake.me
; Permanent URL: http://drushmake.me/file.php?token=1994acf24837
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.
  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 6
  
; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.
  
api = 2
  
; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.
  
; Drupal 7.x. Requires the `core` property to be set to 7.x.

; It is also necessary to define project type to be core as well
projects[drupal][type] = core
  
  
; Modules
; --------

; Modules
projects[views_bulk_operations][version] = "3"

projects[addressfield][version] = "1"

projects[admin_menu][version] = "3"

projects[ais][version] = "1"

projects[auto_entitylabel][version] = "1"

projects[ctools][version] = "1"

projects[calendar][version] = "3"

projects[commerce][version] = "1"

projects[commerce_autosku][version] = "1"

projects[commerce_fieldgroup_panes][version] = "1"

projects[commerce_paypal][version] = "1"

projects[commerce_vbo_views][version] = "1"

projects[context][version] = "3"

projects[date][version] = "2"

projects[devel][version] = "1"

projects[ds][version] = "2"

projects[fontyourface][version] = "2"

projects[email][version] = "1"

projects[entity][version] = "1"

projects[entityreference][version] = "1"

projects[field_group][version] = "1"

projects[formblock][version] = "1.x-dev"

projects[hint][version] = "1"

projects[libraries][version] = "2"

projects[menu_block][version] = "2"

projects[module_filter][version] = "1"

projects[mollom][version] = "2"

projects[overlay_paths][version] = "1"

projects[page_title][version] = "2"

projects[pathauto][version] = "1"

projects[phone][version] = "1.x-dev"

projects[publishcontent][version] = "1"

projects[rules][version] = "2"

projects[stringoverrides][version] = "1"

projects[token][version] = "1"

projects[views][version] = "3"

projects[views_slideshow][version] = "3"

projects[google_analytics][version] = "1"

projects[link][version] = "1"

projects[editablefields][version] = "1"

projects[css_injector][version] = "1"

projects[views_field_view][version] = "1"

projects[commerce_cheque][version] = "1"

projects[commerce_shipping][version] = "2"

projects[path_breadcrumbs][version] = "2"

projects[views_accordion][version] = "1"

projects[menu_attributes][version] = "1"

projects[webform][version] = "4"

projects[wysiwyg][version] = "2"

projects[imce][version] = "1"

projects[imce_wysiwyg][version] = "1"

projects[weight][version] = "2"

projects[r4032login][version] = "1"

projects[commerce_custom_product][version] = "1"

projects[honeypot][version] = "1"

projects[reroute_email][version] = "1"







; FLEXSLIDER IS CALLING ITS OWN MAKEFILE WHICH I COULDNT DO.  DO THIS UNTIL 2 BRANCH IS OUT - THEN UPGRADE AND TEST
projects[flexslider][download][type] = "git"
projects[flexslider][download][url] = "git@gitlab.com:leevh/flexslider-7.x-1.0-rc3.git"
projects[flexslider][type] = "module"


projects[canada_price_format][download][type] = "git"
projects[canada_price_format][download][url] = "git@gitlab.com:leevh/canada-price-format.git"
projects[canada_price_format][type] = "module"


; Themes
projects[omega][subdir] = "sites/all/themes"
projects[omega][version] = "3"


projects[artisan][download][type] = "git"
projects[artisan][download][url] = "git@gitlab.com:leevh/artisanelectricaleducation.ca-theme.git"
projects[artisan][type] = "theme"
projects[artisan][download][branch] = "prod"




; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
projects[mobile_menu_toggle][download][type] = "git"
projects[mobile_menu_toggle][download][url] = "http://git.drupal.org/sandbox/kbasarab/1791874.git"
projects[mobile_menu_toggle][type] = "module"


; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.4/ckeditor_3.6.4.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
libraries[ckfinder][download][type] = "get"
libraries[ckfinder][download][url] = "http://download.cksource.com/CKFinder/CKFinder%20for%20PHP/2.2.2.1/ckfinder_php_2.2.2.1.zip"
libraries[ckfinder][directory_name] = "ckfinder"
libraries[ckfinder][type] = "library"


;REMOVE THIS WHEN UPDATING TO NEW FLEXSLIDER MODULE, IT HAS ITS OWN MAKE FILE FOR THIS.

libraries[flexslider][download][type] = "git"
libraries[flexslider][download][url] = "git@gitlab.com:leevh/flexslider-1.8.git"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"





;NOTES
;---------------------------------
;weform module patched by hand with http://drupal.org/node/1305826#comment-5858208 - please copy webform from local files for now
; sandbox module http://drupal.org/sandbox/kbasarab/1791874
; COMMERCE issue - hacked core in meantime: "commerce_product_ui.module" - line~488 needs to redirect to "admin/course-registration-dates".   Asked in issue http://drupal.org/node/926586#comment-6909468   (have a custom module loaded that doesn't yet solve issue)
; Had to patch Commerce Paypal module with http://drupal.org/node/1301570#comment-6976760 to allow address to be passed along to paypal.  
; CUSTOM module - Canada price formatter, currently in local files for upload.  
